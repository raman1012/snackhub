﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace SnackHubRazorUI
{
    public class GenerateAdminModel : PageModel
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _config;
        private const string adminRole = "Admin";

        public bool RoleCreated { get; set; }
        public string AdminUserId { get; set; }

        public GenerateAdminModel(RoleManager<IdentityRole> roleManager,
                                  UserManager<IdentityUser> userManager,
                                  IConfiguration config)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _config = config;
        }

        /// <summary>
        /// Create the admin role type if it doesnt exist using an email to provide the value 
        /// </summary>
        /// <returns></returns>
        public async Task OnGet()
        {
            //check if admin role exists
            var roleExists = await _roleManager.RoleExistsAsync(adminRole);

            // Create it if it doesnt
            if (roleExists == false)
            {
                await _roleManager.CreateAsync(new IdentityRole(adminRole));
                RoleCreated = true;
            }

            // Get users email id from app config file and then add them to the admin role 
            string userEmail = _config.GetValue<string>("AdminUser");

            var user = await _userManager.FindByEmailAsync(userEmail);

            if (user != null)
            {
                await _userManager.AddToRoleAsync(user, adminRole);
                AdminUserId = user.Id;
            }
        }
    }
}