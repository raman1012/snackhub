﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SnackHubLibrary.Logic;
using SnackHubLibrary.Models;

namespace SnackHubRazorUI
{
    [Authorize]
    public class SnackHubModel : PageModel
    {
        private readonly ISnackHubLogic _snackHub;

        public decimal SnackPrice { get; set; }
        public decimal AmountInserted { get; set; }
        public List<SnackModel> SnackOptions { get; set; }

        [BindProperty]
        public decimal Deposit { get; set; }

        [BindProperty]
        public SnackModel SelectedSnack { get; set; }

        public string UserId { get; set; }

        [BindProperty(SupportsGet = true)]
        public string OutputText { get; set; }

        [BindProperty(SupportsGet = true)]
        public string ErrorMessage { get; set; }

        public SnackHubModel(ISnackHubLogic snackHub)
        {
            _snackHub = snackHub;
        }

        /// <summary>
        /// On startup get all the data we need and authorize a valid user 
        /// </summary>
        public void OnGet()
        {
            UserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            SnackPrice = _snackHub.GetSnackPrice();
            AmountInserted = _snackHub.GetTotalMoneyInserted(UserId);
            SnackOptions = _snackHub.ListTypesOfSnack();
        }

        /// <summary>
        /// Used for depositing change and post the request
        /// </summary>
        /// <returns></returns>

        public IActionResult OnPost()
        {
            UserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (Deposit > 0)
            {
                _snackHub.MoneyInserted(UserId, Deposit);
            }

            return RedirectToPage();
        }

        /// <summary>
        /// Method requests a snack once change has been inserted into the machine
        /// </summary>
        /// <returns></returns>
        public IActionResult OnPostSnack()
        {
            UserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var results = _snackHub.RequestSnack(SelectedSnack, UserId);
            OutputText = "";

            // Handle the response
            if (results.errorMessage.Length > 0)
            {
                ErrorMessage = results.errorMessage;
            }
            else
            {
                OutputText = $"Here is your tasty treat {results.snack.SnackName}. ENJOY!";

                if (results.change.Count > 0)
                {
                    OutputText += "<br><br>Here is your change back. Have a nice day!:<br>";
                    results.change.ForEach(x => OutputText += $"{x.Name}<br>");
                }
                else
                {
                    OutputText += "<br><br>You used exact change. Nothing to refund. Thank you!";
                }
            }

            return RedirectToPage(new { ErrorMessage, OutputText });
        }

        /// <summary>
        /// Issue a refund to the user if transaction is cancelled 
        /// </summary>
        /// <returns></returns>
        public IActionResult OnPostCancel()
        {
            UserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            AmountInserted = _snackHub.GetTotalMoneyInserted(UserId);
            _snackHub.IssueEntireRefund(UserId);

            OutputText = $"You have been refunded {String.Format("{0:C}", AmountInserted)}. Please take your change";

            return RedirectToPage(new { OutputText });
        }
    }
}