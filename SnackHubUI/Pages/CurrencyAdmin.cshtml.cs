using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SnackHubLibrary.Logic;
using SnackHubLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace SnackHubUI.Pages
{
    // Removed for due to demo purposes 
    //[Authorize(Roles ="Admin")]
    public class CurrencyAdminModel : PageModel
    {
        private readonly ISnackHubLogic _snackHub;

        public List<CurrencyModel> Change { get; set; }

        public CurrencyAdminModel(ISnackHubLogic snackHub)
        {
            _snackHub = snackHub;
        }

        [BindProperty]
        public CurrencyModel ChangeToAdd { get; set; } = new CurrencyModel();

        /// <summary>
        /// Admin is able to add change to the inventory
        /// </summary>
        /// <returns></returns>
        public IActionResult OnPost()
        {
            _snackHub.AddMoneyToInventory(new List<CurrencyModel> { ChangeToAdd});
            return RedirectToPage();
        }

        /// <summary>
        /// Get current inventory of the money 
        /// </summary>
        public void OnGet()
        {
            Change = _snackHub.GetChangeInventory().OrderBy(y => y.Name).ToList();
        }
    }
}
