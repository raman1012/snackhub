﻿using Microsoft.Extensions.Configuration;
using SnackHubLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SnackHubLibrary.DataAccess
{
    public class TextFileDataAccess : IDataAccess
    {
        private readonly IConfiguration _config;
        private string currencyTextFile;
        private string snackTextFile;
        private string machineDetailsTextFile;
        private string userChangeTextFile;

        /// <summary>
        /// Flat files for the data
        /// </summary>
        /// <param name="config"></param>
        public TextFileDataAccess(IConfiguration config)
        {
            _config = config;
            currencyTextFile = _config.GetValue<string>("TextFileStorage:Currency");
            snackTextFile = _config.GetValue<string>("TextFileStorage:Snacks");
            machineDetailsTextFile = _config.GetValue<string>("TextFileStorage:MachineDetails");
            userChangeTextFile = _config.GetValue<string>("TextFileStorage:ChangeCredit");
        }

        /// <summary>
        /// Parse the currency text file 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private List<CurrencyModel> RetrieveChange()
        {
            List<CurrencyModel> output = new List<CurrencyModel>();

            var lines = File.ReadAllLines(currencyTextFile);

            try
            {
                foreach (var line in lines)
                {
                    var data = line.Split(',');
                    output.Add(new CurrencyModel
                    {
                        Name = data[0],
                        Amount = decimal.Parse(data[1])
                    });
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                throw new Exception("Missing data in the currency text file.", ex);
            }
            catch (FormatException ex)
            {
                throw new Exception("The data in the currency text file has been corrupted.", ex);
            }
            catch
            {
                throw;
            }

            return output;
        }

        /// <summary>
        /// Save data to currency text file 
        /// </summary>
        /// <param name="coins"></param>
        private void SaveChangeToTextFile(List<CurrencyModel> coins)
        {
            File.WriteAllLines(currencyTextFile, coins.Select(c => $"{c.Name},{c.Amount}"));
        }

        /// <summary>
        /// Adds more money to the change inventory via admin role and to the file
        /// </summary>
        /// <param name="coins"></param>
        public void CurrencyInventory_AddChange(List<CurrencyModel> coins)
        {
            coins.AddRange(RetrieveChange());

            SaveChangeToTextFile(coins);
        }

        /// <summary>
        /// Get all the change data currently available 
        /// </summary>
        /// <returns></returns>
        public List<CurrencyModel> CurrencyInventory_GetAll()
        {
            return RetrieveChange();
        }

        /// <summary>
        /// Get current change inventory remove the change that needs to be returned to user
        /// And then save it to file with new value 
        /// </summary>
        /// <param name="coinValue"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public List<CurrencyModel> CurrencyInventory_WithdrawChange(decimal coinValue, int quantity)
        {
            var coins = RetrieveChange();

            var changeToReturn = coins.Where(x => x.Amount == coinValue).Take(quantity).ToList();

            changeToReturn.ForEach(x => coins.Remove(x));

            SaveChangeToTextFile(coins);

            return changeToReturn;
        }

        /// <summary>
        /// Parse the snack machine data ffrom the text file
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private (decimal snackPrice, decimal changeOnHand, decimal totalIncome) RetrieveMachineDetails()
        {
            (decimal snackPrice, decimal cashOnHand, decimal totalIncome) output;
            var lines = File.ReadAllLines(machineDetailsTextFile);

            try
            {
                output.snackPrice = decimal.Parse(lines[0]);
                output.cashOnHand = decimal.Parse(lines[1]);
                output.totalIncome = decimal.Parse(lines[2]);
            }
            catch (IndexOutOfRangeException ex)
            {
                throw new Exception("Missing data in the Machine Details file.", ex);
            }
            catch (FormatException ex)
            {
                throw new Exception("The data in the Machine Details file has been ruined.", ex);
            }
            catch
            {
                throw;
            }

            return output;
        }

        /// <summary>
        /// Add new machine info to the text file
        /// </summary>
        /// <param name="snackPrice"></param>
        /// <param name="cashOnHand"></param>
        /// <param name="totalIncome"></param>
        private void SaveMachineInfoToTextFile(decimal snackPrice, decimal cashOnHand, decimal totalIncome)
        {
            string[] infoToSave = new string[] { snackPrice.ToString(), cashOnHand.ToString(), totalIncome.ToString() };

            File.WriteAllLines(machineDetailsTextFile, infoToSave);
        }

        /// <summary>
        /// Admin ability to check how much cash is currently in the machine
        /// </summary>
        /// <returns></returns>
        public decimal MachineDetail_CashOnHand()
        {
            var info = RetrieveMachineDetails();

            return info.changeOnHand;
        }

        /// <summary>
        /// Withdraw all the cash from the machine
        /// </summary>
        /// <returns></returns>
        public decimal MachineDetail_EmptyCash()
        {
            var info = RetrieveMachineDetails();

            SaveMachineInfoToTextFile(info.snackPrice, 0, info.totalIncome);

            return info.changeOnHand;
        }

        /// <summary>
        /// Check the price of a snack
        /// </summary>
        /// <returns></returns>
        public decimal MachineDetail_SnackPrice()
        {
            var info = RetrieveMachineDetails();

            return info.snackPrice;
        }

        /// <summary>
        /// Check the price of a snack
        /// </summary>
        /// <returns></returns>
        public decimal MachineDetail_TotalIncome()
        {
            var info = RetrieveMachineDetails();

            return info.totalIncome;
        }

        /// <summary>
        /// Parse all the snacks data from the text file
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public List<SnackModel> RetrieveSnacks()
        {
            List<SnackModel> output = new List<SnackModel>();
            var lines = File.ReadAllLines(snackTextFile);

            try
            {
                foreach (var line in lines)
                {
                    var data = line.Split(',');
                    output.Add(new SnackModel
                    {
                        SnackName = data[0],
                        OccupiedSlot = data[1]
                    });
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                throw new Exception("Missing data from snack text file.", ex);
            }
            catch
            {
                throw;
            }

            return output;
        }

        /// <summary>
        /// Add new rows to snacks text file
        /// </summary>
        /// <param name="snacks"></param>
        public void SaveSnacksToTextFile(List<SnackModel> snacks)
        {
            File.WriteAllLines(snackTextFile, snacks.Select(s => $"{s.SnackName},{s.OccupiedSlot}"));
        }

        /// <summary>
        /// Add snacks to inventory and then add them to file
        /// </summary>
        /// <param name="snacks"></param>
        public void SnackInventory_AddSnacks(List<SnackModel> snacks)
        {
            snacks.AddRange(RetrieveSnacks());
            SaveSnacksToTextFile(snacks);
        }

        public bool SnackInventory_CheckIfSnackIsInStock(SnackModel snack)
        {
            var snacks = RetrieveSnacks();
            var outputSnack = snacks.Where(x => x.SnackName == snack.SnackName && x.OccupiedSlot == snack.OccupiedSlot).FirstOrDefault();

            return outputSnack != null;
        }

        /// <summary>
        /// Get all the snacks from snack text file
        /// </summary>
        /// <returns></returns>
        public List<SnackModel> SnackInventory_GetAll()
        {
            return RetrieveSnacks();
        }

        public SnackModel SnackInventory_GetSnack(SnackModel snack, decimal amount)
        {
            var snacks = RetrieveSnacks();
            var outputSnack = snacks.Where(x => x.SnackName == snack.SnackName && x.OccupiedSlot == snack.OccupiedSlot).FirstOrDefault();

            if (outputSnack != null)
            {
                var info = RetrieveMachineDetails();
                info.changeOnHand += amount;
                info.totalIncome += amount;
                SaveMachineInfoToTextFile(info.snackPrice, info.changeOnHand, info.totalIncome);
            }

            return outputSnack;
        }

        /// <summary>
        /// Group snacks by type
        /// </summary>
        /// <returns></returns>
        public List<SnackModel> SnackInventory_GetTypes()
        {
            var snacks = RetrieveSnacks();

            return snacks.GroupBy(x => x.SnackName).Select(x => x.First()).ToList();
        }

        /// <summary>
        /// Parse the user change data text file
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public Dictionary<string, decimal> RetrieveUserChange()
        {
            Dictionary<string, decimal> output = new Dictionary<string, decimal>();
            var lines = File.ReadAllLines(userChangeTextFile);

            try
            {
                foreach (var line in lines)
                {
                    var data = line.Split(',');
                    output.Add(data[0], decimal.Parse(data[1]));
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                throw new Exception("Missing data in the User Change text file.", ex);
            }
            catch (FormatException ex)
            {
                throw new Exception("The data in the User Change text file has been ruined.", ex);
            }
            catch
            {
                throw;
            }

            return output;
        }

        /// <summary>
        /// Save the user chanage data to text file
        /// </summary>
        /// <param name="credits"></param>
        public void SaveUserChange(Dictionary<string, decimal> credits)
        {
            File.WriteAllLines(userChangeTextFile, credits.Select(c => $"{c.Key},{c.Value}"));
        }

        /// <summary>
        /// zero out the users credit 
        /// </summary>
        /// <param name="userId"></param>
        public void UserChange_Clear(string userId)
        {
            var credits = RetrieveUserChange();
            credits[userId] = 0;
            SaveUserChange(credits);
        }

        /// <summary>
        /// validate legit user then accept the user change to purchase the snack and validate 
        /// we have enough change to give 
        /// </summary>
        /// <param name="userId"></param>
        /// <exception cref="Exception"></exception>
        public void UserChange_DepositedIntoMachine(string userId)
        {
            // check current change inventory from the file
            var userChange = RetrieveUserChange();

            // has to be a valid user to buy a snack
            if (userChange.ContainsKey(userId) == false)
            {
                throw new Exception("User not found for deposit");
            }

            var info = RetrieveMachineDetails();
            info.changeOnHand += userChange[userId];
            info.totalIncome += userChange[userId];

            SaveMachineInfoToTextFile(info.snackPrice, info.changeOnHand, info.totalIncome);

            userChange.Remove(userId);

            SaveUserChange(userChange);
        }

        /// <summary>
        /// Keep track of user change during transaction
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="amount"></param>
        public void UserChange_Insert(string userId, decimal amount)
        {
            var credits = RetrieveUserChange();

            credits.TryGetValue(userId, out decimal currentCredit);
            credits[userId] = currentCredit + amount;

            SaveUserChange(credits);
        }

        /// <summary>
        /// Track the total amount of change inserted into the machine not deposited
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public decimal UserChange_Total(string userId)
        {
            var change = RetrieveUserChange();

            change.TryGetValue(userId, out decimal currentChange);
            return currentChange;
        }
    }
}
