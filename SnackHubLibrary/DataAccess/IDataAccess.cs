﻿using SnackHubLibrary.Models;
using System.Collections.Generic;

namespace SnackHubLibrary.DataAccess
{
    public interface IDataAccess
    {
        List<SnackModel> SnackInventory_GetTypes();
        bool SnackInventory_CheckIfSnackIsInStock(SnackModel snack);
        SnackModel SnackInventory_GetSnack(SnackModel snack, decimal amount);
        void SnackInventory_AddSnacks(List<SnackModel> snacks);
        List<SnackModel> SnackInventory_GetAll();
        void UserChange_Insert(string userId, decimal amount);
        void UserChange_Clear(string userId);
        decimal UserChange_Total(string userId);
        void UserChange_DepositedIntoMachine(string userId);
        decimal MachineDetail_SnackPrice();
        decimal MachineDetail_EmptyCash();
        decimal MachineDetail_CashOnHand();
        decimal MachineDetail_TotalIncome();
        List<CurrencyModel> CurrencyInventory_WithdrawChange(decimal coinValue, int quantity);
        List<CurrencyModel> CurrencyInventory_GetAll();
        void CurrencyInventory_AddChange(List<CurrencyModel> coins);

    }
}
