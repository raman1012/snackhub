﻿using SnackHubLibrary.Models;
using System.Collections.Generic;

namespace SnackHubLibrary.Logic
{
    public interface ISnackHubLogic
    {
        List<SnackModel> ListTypesOfSnack();

        // Takes in an amount and returns the total inserted so far
        decimal MoneyInserted(string userId, decimal monetaryAmount);

        // Gets the overall price for snacks - there is no individual pricing
        decimal GetSnackPrice();

        (SnackModel snack, List<CurrencyModel> change, string errorMessage) RequestSnack(SnackModel snack, string userId);

        void IssueEntireRefund(string userId);

        decimal GetTotalMoneyInserted(string userId);

        void AddToSnackInventory(List<SnackModel> snacks);

        List<SnackModel> GetSnackInventory();

        decimal EmptyMoneyFromTheMachine();

        List<CurrencyModel> GetChangeInventory();

        void AddMoneyToInventory(List<CurrencyModel> coins);

        // Revenue for the current day
        decimal GetCurrentRevenue();

        decimal GetTotalProfit();
    }
}
