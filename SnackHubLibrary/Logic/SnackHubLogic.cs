﻿using SnackHubLibrary.DataAccess;
using SnackHubLibrary.Models;
using System;
using System.Collections.Generic;

namespace SnackHubLibrary.Logic
{
    public class SnackHubLogic : ISnackHubLogic
    {
        private readonly IDataAccess _dataAccess;

        public SnackHubLogic(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public void AddMoneyToInventory(List<CurrencyModel> coins)
        {
            _dataAccess.CurrencyInventory_AddChange(coins);
        }

        public void AddToSnackInventory(List<SnackModel> snacks)
        {
            _dataAccess.SnackInventory_AddSnacks(snacks);
        }

        public decimal EmptyMoneyFromTheMachine()
        {
            return _dataAccess.MachineDetail_EmptyCash();
        }

        public List<CurrencyModel> GetChangeInventory()
        {
            return _dataAccess.CurrencyInventory_GetAll();
        }

        public decimal GetCurrentRevenue()
        {
            return _dataAccess.MachineDetail_CashOnHand();
        }

        public decimal GetTotalMoneyInserted(string userId)
        {
            return _dataAccess.UserChange_Total(userId);
        }

        public List<SnackModel> GetSnackInventory()
        {
            return _dataAccess.SnackInventory_GetAll();
        }

        public decimal GetSnackPrice()
        {
            return _dataAccess.MachineDetail_SnackPrice();
        }

        public decimal GetTotalProfit()
        {
            return _dataAccess.MachineDetail_TotalIncome();
        }

        public void IssueEntireRefund(string userId)
        {
            _dataAccess.UserChange_Clear(userId);
        }

        public List<SnackModel> ListTypesOfSnack()
        {
            return _dataAccess.SnackInventory_GetTypes();
        }

        public decimal MoneyInserted(string userId, decimal cashAmount)
        {
            _dataAccess.UserChange_Insert(userId, cashAmount);

            return _dataAccess.UserChange_Total(userId);
        }

        public (SnackModel snack, List<CurrencyModel> change, string errorMessage) RequestSnack(SnackModel snack, string userId)
        {
            (SnackModel snack, List<CurrencyModel> change, string errorMessage) output = (null, new List<CurrencyModel>(), "OOPS...Snack Hub Had An unexpected error occurred");
            decimal userCredit = _dataAccess.UserChange_Total(userId);
            decimal snackCost = _dataAccess.MachineDetail_SnackPrice();

            if (userCredit >= snackCost) 
            {
                bool IsSnackInStock = _dataAccess.SnackInventory_CheckIfSnackIsInStock(snack);

                if (IsSnackInStock == false)
                {
                    output.errorMessage = "Snack is out of stock.";
                }
                else
                {
                    try
                    {
                        var change = GetChange(snackCost, userCredit);

                        var snackToReturn = _dataAccess.SnackInventory_GetSnack(snack, userCredit);
                        _dataAccess.UserChange_Clear(userId);
                        output = (snackToReturn, change, "");
                    }
                    catch (Exception ex)
                    {
                        output.errorMessage = "There is not enough money to give you change.";
                    }
                }
            }
            else // Not enough money inserted WOMP WOMP
            {
                output = (null, new List<CurrencyModel>(), "Customer did not provide enough change.");
            }

            return output;
        }

        private List<CurrencyModel> GetChange(decimal snackCost, decimal userCredit)
        {
            decimal difference = userCredit - snackCost;
            List<CurrencyModel> output = new List<CurrencyModel>();

            if (difference > 0)
            {
                int quartersCount = (int)Math.Floor(difference / 0.25M);
                var quarters = _dataAccess.CurrencyInventory_WithdrawChange(0.25M, quartersCount);
                output.AddRange(quarters);
                difference -= (quarters.Count * 0.25M);

                int dimesCount = (int)Math.Floor(difference / 0.1M);
                var dimes = _dataAccess.CurrencyInventory_WithdrawChange(0.1M, dimesCount);
                output.AddRange(dimes);
                difference -= (dimes.Count * 0.1M);

                int nicklesCount = (int)Math.Floor(difference / 0.05M);
                var nickles = _dataAccess.CurrencyInventory_WithdrawChange(0.05M, nicklesCount);
                output.AddRange(nickles);
                difference -= (nickles.Count * 0.05M);

                if (difference > 0)
                {
                    throw new Exception("OH NO...Could not make proper change. I FEEL SILLY");
                }
            }

            return output;
        }
    }
}
