﻿namespace SnackHubLibrary.Models
{
    public class SnackModel
    {
        public string SnackName { get; set; }
        public string OccupiedSlot { get; set; }
    }
}
