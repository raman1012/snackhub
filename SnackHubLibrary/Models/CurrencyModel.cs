﻿namespace SnackHubLibrary.Models
{
    public class CurrencyModel
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
}
