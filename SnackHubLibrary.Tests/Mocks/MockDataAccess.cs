﻿using SnackHubLibrary.DataAccess;
using SnackHubLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SnackHubLibrary.Tests.Mocks
{
    public class MockDataAccess : IDataAccess
    {
        public List<CurrencyModel> ChangeInventory { get; set; } = new List<CurrencyModel>();
        public (decimal snackPrice, decimal cashOnHand, decimal totalIncome) MachineDetail { get; set; }
        public List<SnackModel> SnackInventory { get; set; } = new List<SnackModel>();
        public Dictionary<string, decimal> UserChange { get; set; } = new Dictionary<string, decimal>();

        public MockDataAccess()
        {
            ChangeInventory.Add(new CurrencyModel { Name = "Quarter", Amount = 0.25M });
            ChangeInventory.Add(new CurrencyModel { Name = "Quarter", Amount = 0.25M });
            ChangeInventory.Add(new CurrencyModel { Name = "Quarter", Amount = 0.25M });
            ChangeInventory.Add(new CurrencyModel { Name = "Quarter", Amount = 0.25M });
            ChangeInventory.Add(new CurrencyModel { Name = "Dime", Amount = 0.1M });
            ChangeInventory.Add(new CurrencyModel { Name = "Dime", Amount = 0.1M });
            ChangeInventory.Add(new CurrencyModel { Name = "Nickle", Amount = 0.05M });
            ChangeInventory.Add(new CurrencyModel { Name = "Nickle", Amount = 0.05M });
            ChangeInventory.Add(new CurrencyModel { Name = "Nickle", Amount = 0.05M });
            ChangeInventory.Add(new CurrencyModel { Name = "Nickle", Amount = 0.05M });
            ChangeInventory.Add(new CurrencyModel { Name = "Nickle", Amount = 0.05M });

            MachineDetail = (0.75M, 25.65M, 201.50M);

            SnackInventory.Add(new SnackModel { SnackName = "Reese's Cups", OccupiedSlot = "1" });
            SnackInventory.Add(new SnackModel { SnackName = "Reese's Cups", OccupiedSlot = "1" });
            SnackInventory.Add(new SnackModel { SnackName = "Reese's Cups", OccupiedSlot = "1" });
            SnackInventory.Add(new SnackModel { SnackName = "Reese's Cups", OccupiedSlot = "1" });
            SnackInventory.Add(new SnackModel { SnackName = "Reese's Cups", OccupiedSlot = "1" });
            SnackInventory.Add(new SnackModel { SnackName = "Butterfinger", OccupiedSlot = "2" });
            SnackInventory.Add(new SnackModel { SnackName = "Hershey's Kisses", OccupiedSlot = "3" });
            SnackInventory.Add(new SnackModel { SnackName = "Hershey's Kisses", OccupiedSlot = "3" });
        }

        public void CurrencyInventory_AddChange(List<CurrencyModel> coins)
        {
            ChangeInventory.AddRange(coins);
        }

        public List<CurrencyModel> CurrencyInventory_GetAll()
        {
            return ChangeInventory;
        }

        public List<CurrencyModel> CurrencyInventory_WithdrawChange(decimal changeValue, int quantity)
        {
            var coins = ChangeInventory.Where(x => x.Amount == changeValue).Take(quantity).ToList();

            coins.ForEach(x => ChangeInventory.Remove(x));

            return coins;
        }

        public decimal MachineDetail_CashOnHand()
        {
            return MachineDetail.cashOnHand;
        }

        public decimal MachineDetail_EmptyCash()
        {
            var val = MachineDetail;
            decimal output = val.cashOnHand;

            val.cashOnHand = 0;
            MachineDetail = val;

            return output;
        }

        public decimal MachineDetail_SnackPrice()
        {
            return MachineDetail.snackPrice;
        }

        public decimal MachineDetail_TotalIncome()
        {
            return MachineDetail.totalIncome;
        }

        public void SnackInventory_AddSnacks(List<SnackModel> snacks)
        {
            SnackInventory.AddRange(snacks);
        }

        public List<SnackModel> SnackInventory_GetAll()
        {
            return SnackInventory;
        }

        public bool SnackInventory_CheckIfSnackIsInStock(SnackModel snack)
        {
            var outputSnack = SnackInventory.Where(x => x.SnackName == snack.SnackName).FirstOrDefault();

            return outputSnack != null;
        }

        public SnackModel SnackInventory_GetSnack(SnackModel snack, decimal amount)
        {
            var outputSnack = SnackInventory.Where(x => x.SnackName == snack.SnackName).FirstOrDefault();

            if (outputSnack != null)
            {
                var info = MachineDetail;

                info.cashOnHand += amount;

                info.totalIncome += amount;

                MachineDetail = info; 
            }

            return outputSnack;
        }

        public List<SnackModel> SnackInventory_GetTypes()
        {
            return SnackInventory.GroupBy(x => x.SnackName).Select(x => x.First()).ToList();
        }

        public void UserChange_Clear(string userId)
        {
            if (UserChange.ContainsKey(userId))
            {
                UserChange[userId] = 0;
            }
        }

        public void UserChange_DepositedIntoMachine(string userId)
        {
            if (UserChange.ContainsKey(userId) == false)
            {
                throw new Exception("Customer not found for deposit");
            }

            var info = MachineDetail;

            info.cashOnHand += UserChange[userId]
                ;
            info.totalIncome += UserChange[userId];

            UserChange.Remove(userId);
        }

        public void UserChange_Insert(string userId, decimal amount)
        {
            UserChange.TryGetValue(userId, out decimal currentCredit);

            UserChange[userId] = currentCredit + amount;
        }

        public decimal UserChange_Total(string userId)
        {
            UserChange.TryGetValue(userId, out decimal currentCredit);

            return currentCredit;
        }
    }
}
