using Xunit;
using SnackHubLibrary.Logic;
using SnackHubLibrary.Models;
using SnackHubLibrary.Tests.Mocks;
using System.Collections.Generic;
using System.Linq;

namespace SnackHubLibrary.Tests
{
    public class SnackHubLogicTests
    {
        [Fact]
        public void AddToCoinInventory_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);
            List<CurrencyModel> coins = new List<CurrencyModel>
            {
                new CurrencyModel{ Name = "Quarter", Amount = 0.25M},
                new CurrencyModel{ Name = "Quarter", Amount = 0.25M},
                new CurrencyModel{ Name = "Dime", Amount = 0.1M},
            };

            logic.AddMoneyToInventory(coins);

            int expected = 6;
            int actual = da.ChangeInventory.Where(x => x.Name == "Quarter").Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddToSnackInventory_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            List<SnackModel> snacks = new List<SnackModel>
            {
                new SnackModel{ SnackName = "Reese's Cups", OccupiedSlot = "1"},
                new SnackModel{ SnackName = "Reese's Cups", OccupiedSlot = "1"}
            };

            logic.AddToSnackInventory(snacks);

            int expected = 1;
            int actual = da.SnackInventory_GetTypes().Where(x => x.SnackName == "Reese's Cups").Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EmptyMoneyFromMachine_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            var (_, expected, _) = da.MachineDetail;
            decimal actual = logic.EmptyMoneyFromTheMachine();

            Assert.Equal(expected, actual);

            expected = 0;
            (_, actual, _) = da.MachineDetail;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetCoinInventory_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            var coins = logic.GetChangeInventory();

            int expected = da.ChangeInventory.Where(x => x.Name == "Quarter").Count();
            int actual = coins.Where(x => x.Name == "Quarter").Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetCurrentIncome_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            var (_, expected, _) = da.MachineDetail;
            decimal actual = logic.GetCurrentRevenue();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetMoneyInsertedTotal_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            decimal expected = 0.65M;
            da.UserChange.Add("test", expected);

            decimal actual = logic.GetTotalMoneyInserted("test");

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetSnackInventory_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            int actual = logic.GetSnackInventory().Count();
            int expected = 8;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetSnackPrice_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            decimal expected = da.MachineDetail.snackPrice;
            decimal actual = logic.GetSnackPrice();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetTotalIncome_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            decimal expected = da.MachineDetail.totalIncome;
            decimal actual = logic.GetTotalProfit();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void IssueFullRefund_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);
            string user = "test";

            da.UserChange[user] = 0.65M;
            
            logic.IssueEntireRefund(user);

            Assert.True(da.UserChange[user] == 0);
        }

        [Fact]
        public void ListTypesOfSnack_ShouldWork()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            int expected = 3;
            int actual = logic.ListTypesOfSnack().Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MoneyInsert_SingleUserShouldHaveCorrectAmount()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);
            string user = "test";
            decimal money = 0.25M;

            logic.MoneyInserted(user, money);

            decimal expected = money;
            decimal actual = da.UserChange[user];

            Assert.Equal(expected, actual);

            logic.MoneyInserted(user, money);

            expected += money;
            actual = da.UserChange[user];

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MoneyInsert_User2ShouldHaveCorrectAmount()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);
            string user1 = "test";
            string user2 = "tim";
            decimal money = 0.25M;
            decimal user2Money = 0.10M;

            logic.MoneyInserted(user1, money);
            logic.MoneyInserted(user2, user2Money);
            logic.MoneyInserted(user1, money);

            decimal expected = user2Money;
            decimal actual = da.UserChange[user2];

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RequestSnack_ShouldReturnSnackWithNoChange()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            string user = "test";
            SnackModel expectedSnack = new SnackModel { SnackName = "Reese's Cups", OccupiedSlot = "1" };
            var initialState = da.MachineDetail;

            da.UserChange[user] = 0.75M;

            var results = logic.RequestSnack(expectedSnack, user);

            Assert.Equal(expectedSnack.SnackName, results.snack.SnackName);
            Assert.Equal(expectedSnack.OccupiedSlot, results.snack.OccupiedSlot);

            Assert.Equal(0, da.UserChange[user]);

            Assert.Equal(initialState.cashOnHand + 0.75M, da.MachineDetail.cashOnHand);
            Assert.Equal(initialState.totalIncome + 0.75M, da.MachineDetail.totalIncome);

            Assert.True(string.IsNullOrWhiteSpace(results.errorMessage));

            Assert.True(results.change.Count() == 0);
        }

        [Fact]
        public void RequestSnack_ShouldSayNotEnoughMoney()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            string user = "test";
            SnackModel expectedSnack = new SnackModel { SnackName = "Coke", OccupiedSlot = "1" };
            var initialState = da.MachineDetail;

            da.UserChange[user] = 0.5M;

            var results = logic.RequestSnack(expectedSnack, user);

            Assert.Null(results.snack);

            Assert.Equal(0.5M, da.UserChange[user]);

            Assert.Equal(initialState.cashOnHand, da.MachineDetail.cashOnHand);
            Assert.Equal(initialState.totalIncome, da.MachineDetail.totalIncome);

            Assert.False(string.IsNullOrWhiteSpace(results.errorMessage));

            Assert.True(results.change.Count() == 0);
        }

        [Fact]
        public void RequestSnack_ShouldSayOutOfStock()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            string user = "test";
            SnackModel expectedSnack = new SnackModel { SnackName = "Fanta", OccupiedSlot = "4" };
            var initialState = da.MachineDetail;

            da.UserChange[user] = 0.75M;

            var results = logic.RequestSnack(expectedSnack, user);

            Assert.Null(results.snack);

            Assert.Equal(0.75M, da.UserChange[user]);

            Assert.Equal(initialState.cashOnHand, da.MachineDetail.cashOnHand);
            Assert.Equal(initialState.totalIncome, da.MachineDetail.totalIncome);

            Assert.False(string.IsNullOrWhiteSpace(results.errorMessage));

            Assert.True(results.change.Count() == 0);
        }

        [Fact]
        public void RequestSnack_ShouldSayNotEnoughChange()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            string user = "test";
            SnackModel expectedSnack = new SnackModel { SnackName = "Coke", OccupiedSlot = "1" };
            var initialState = da.MachineDetail;

            da.UserChange[user] = 1.75M;
            da.ChangeInventory.Clear();

            var results = logic.RequestSnack(expectedSnack, user);

            Assert.Null(results.snack);

            Assert.Equal(1.75M, da.UserChange[user]);

            Assert.Equal(initialState.cashOnHand, da.MachineDetail.cashOnHand);
            Assert.Equal(initialState.totalIncome, da.MachineDetail.totalIncome);

            Assert.False(string.IsNullOrWhiteSpace(results.errorMessage));

            Assert.True(results.change.Count() == 0);
        }

        [Fact]
        public void RequestSnack_ShouldReturnSnackWithChange()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            string user = "test";
            SnackModel expectedSnack = new SnackModel { SnackName = "Reese's Cups", OccupiedSlot = "1" };
            var initialState = da.MachineDetail;

            da.UserChange[user] = 1M;

            var results = logic.RequestSnack(expectedSnack, user);

            Assert.Equal(expectedSnack.SnackName, results.snack.SnackName);
            Assert.Equal(expectedSnack.OccupiedSlot, results.snack.OccupiedSlot);

            Assert.Equal(0, da.UserChange[user]);

            Assert.Equal(initialState.cashOnHand + 1M, da.MachineDetail.cashOnHand);
            Assert.Equal(initialState.totalIncome + 1M, da.MachineDetail.totalIncome);

            Assert.True(string.IsNullOrWhiteSpace(results.errorMessage));

            Assert.True(results.change.Count() == 1);
        }

        [Fact]
        public void RequestSnack_ShouldReturnSnackWithAlternateChange()
        {
            MockDataAccess da = new MockDataAccess();
            SnackHubLogic logic = new SnackHubLogic(da);

            string user = "test";
            SnackModel expectedSnack = new SnackModel { SnackName = "Reese's Cups", OccupiedSlot = "1" };
            var initialState = da.MachineDetail;

            da.UserChange[user] = 1M;
            da.ChangeInventory.RemoveAll(x => x.Amount == 0.25M);

            var results = logic.RequestSnack(expectedSnack, user);

            Assert.Equal(expectedSnack.SnackName, results.snack.SnackName);
            Assert.Equal(expectedSnack.OccupiedSlot, results.snack.OccupiedSlot);

            Assert.Equal(0, da.UserChange[user]);

            Assert.Equal(initialState.cashOnHand + 1M, da.MachineDetail.cashOnHand);
            Assert.Equal(initialState.totalIncome + 1M, da.MachineDetail.totalIncome);

            Assert.True(string.IsNullOrWhiteSpace(results.errorMessage));

            // Dime + Dime + Nickle = Quarter
            Assert.True(results.change.Count() == 3);
        }
    }
}
